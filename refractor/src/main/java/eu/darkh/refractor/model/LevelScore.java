package eu.darkh.refractor.model;

import org.joda.time.DateTime;

/**
 * Created by darkh on 2015-02-15.
 */
public class LevelScore {

    private int levelId;
    private long timestamp;
    private int stars;
    private long playTime;

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public long getPlayTime() {
        return playTime;
    }

    public void setPlayTime(long playTime) {
        this.playTime = playTime;
    }
}
