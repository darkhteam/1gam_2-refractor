package eu.darkh.refractor.enums;

/**
 * Created by darkh on 2015-02-14.
 */
public enum LevelObjectType {
    LASER_EMITTER,
    LASER_RECEIVER,
    MIRROR,
    OBSTACLE
}
