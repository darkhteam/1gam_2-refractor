package eu.darkh.refractor.scenes;

import android.util.Log;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.util.GLState;

import eu.darkh.refractor.consts.SceneConstants;
import eu.darkh.refractor.managers.SceneManager;

/**
 * Created by darkh on 2015-02-14.
 */
public class SplashScene extends BaseScene {

    private ITextureRegion splash_region;
    private ITiledTextureRegion play_button_region;

    private Sprite splash;
    private ButtonSprite playButton;

    @Override
    public void onBackKeyPressed() {
        activity.finish();
    }

    @Override
    protected void createScene() {
        setTouchAreaBindingOnActionDownEnabled(true);
        createSplash();
        createPlayButton();
    }

    private void createSplash() {
        splash = new Sprite(300, 300, splash_region, vbom)
        {
            @Override
            protected void preDraw(GLState pGLState, Camera pCamera)
            {
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }
        };

        splash.setPosition(250, 50);
        attachChild(splash);
    }

    private void createPlayButton() {
        playButton = new ButtonSprite(200, 50, play_button_region, vbom) {
            @Override
            public boolean onAreaTouched(TouchEvent touchEvent, float touchAreaLocalX, float touchAreaLocalY) {
                loadMenuScene();
                return super.onAreaTouched(touchEvent, touchAreaLocalX, touchAreaLocalY);
            }
        };

        playButton.setPosition(250, 400);
        registerTouchArea(playButton);
        attachChild(playButton);
    }

    private void loadMenuScene() {
        SceneManager.getInstance().createLevelMenuScene();
    }

    @Override
    protected void disposeScene() {
        splash.detachSelf();
        splash.dispose();
        playButton.detachSelf();
        playButton.dispose();
        this.detachSelf();
        this.dispose();
    }

    @Override
    protected void loadResources() {
       loadTextures();
    }

    private void loadTextures() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/splash/");
        textureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), SceneConstants.SCENE_WIDTH, SceneConstants.SCENE_HEIGHT, TextureOptions.BILINEAR);
        splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "splash.png");
        play_button_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, activity, "play.png", 2, 1);

        try {
            textureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
            textureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Log.e("Ref", "So much error splasz scene!", e);
        }
    }

    @Override
    protected void unloadResources() {
        textureAtlas.unload();
        splash_region = null;
        play_button_region = null;
    }
}
