package eu.darkh.refractor.scenes;

import android.graphics.Rect;
import android.util.Log;
import android.util.Pair;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.SAXUtils;
import org.andengine.util.color.Color;
import org.andengine.util.level.IEntityLoader;
import org.andengine.util.level.LevelLoader;
import org.andengine.util.level.constants.LevelConstants;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.xml.sax.Attributes;

import java.io.IOException;
import java.util.ArrayList;

import eu.darkh.refractor.consts.EntityConstants;
import eu.darkh.refractor.consts.SceneConstants;
import eu.darkh.refractor.enums.LevelObjectType;
import eu.darkh.refractor.managers.GameSaveManager;
import eu.darkh.refractor.managers.SceneManager;
import eu.darkh.refractor.model.LevelScore;

import static eu.darkh.refractor.utils.MathUtils.calculateReflectionAngle;
import static eu.darkh.refractor.utils.MathUtils.translatePoint;

/**
 * Created by darkh on 2015-02-14.
 */
public class GameScene extends BaseScene {

    private static final float DEFAULT_LASER_WIDTH = 50;
    private static final int DEFAULT_TRIES_COUNT = 3;

    private int levelId;

    private ITextureRegion laser_emitter_region;
    private ITextureRegion laser_receiver_region;
    private ITextureRegion mirror_region;
    private ITextureRegion obstacle1_region;

    private ITiledTextureRegion activate_laser_button_region;
    private ITiledTextureRegion restart_level_button_region;
    private ITiledTextureRegion level_list_button_region;

    private ButtonSprite activateLaserButton;
    private ButtonSprite restartLevelButton;
    private ButtonSprite levelMenuButton;
    private Font font;

    private float laserEmitterX;
    private float laserEmitterY;

    private float laserReceiverX;
    private float laserReceiverY;
    private float laserEmitterAngle;

    private ArrayList<Line> laser = new ArrayList<>();
    private ArrayList<Sprite> levelObjects = new ArrayList<>();

    private HUD gameHUD;

    private Text levelText;
    private Text triesText;
    private Text missText;
    private int tryCounter = DEFAULT_TRIES_COUNT;

    private DateTime gameStartTime;
    private Duration gameDuration;

    private Rectangle overlay;

    public GameScene(int levelId) {
        super();
        this.levelId = levelId;
    }

    @Override
    public void onBackKeyPressed() {
        goBack();
    }

    private void goBack() {
        SceneManager.getInstance().loadLevelMenuScene();
    }

    @Override
    protected void createScene() {
        createBackground();
        createHUD();
        createButtons();
        configureScene();
        loadLevel();
        startGame();
    }



    private void createBackground() {
        setBackground(new Background(Color.WHITE));
    }

    private void createHUD() {
        gameHUD = new HUD();

        levelText = new Text(10, 10, font, "Level: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
        levelText.setText("Level: " + (levelId + 1));
        gameHUD.attachChild(levelText);

        triesText = new Text(600, 10, font, "Tries left: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
        updateTriesText();
        gameHUD.attachChild(triesText);

        camera.setHUD(gameHUD);
    }

    private void updateTriesText() {
        triesText.setText("Tries left: " + tryCounter);
    }

    private void createButtons() {
        restartLevelButton = createRestartLevelButton();
        attachChild(restartLevelButton);

        levelMenuButton = createLevelMenuButton();
        attachChild(levelMenuButton);

        activateLaserButton = createActivateLaserButton();
        attachChild(activateLaserButton);
    }

    private ButtonSprite createRestartLevelButton() {
        ButtonSprite restartLevelButton = new ButtonSprite(200, 100, restart_level_button_region, vbom);
        restartLevelButton.setOnClickListener(new ButtonSprite.OnClickListener() {
            @Override
            public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                restartLevel();
            }
        });

        restartLevelButton.setPosition(650, 100);
        restartLevelButton.setWidth(50);
        restartLevelButton.setHeight(50);
        registerTouchArea(restartLevelButton);
        return restartLevelButton;
    }

    private void restartLevel() {
        SceneManager.getInstance().restartLevel(levelId, engine);
    }

    private ButtonSprite createLevelMenuButton() {
        ButtonSprite levelMenuButton = new ButtonSprite(200, 100, level_list_button_region, vbom);
        levelMenuButton.setOnClickListener(new ButtonSprite.OnClickListener() {
            @Override
            public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                goBack();
            }
        });

        levelMenuButton.setPosition(725, 100);
        levelMenuButton.setWidth(50);
        levelMenuButton.setHeight(50);
        registerTouchArea(levelMenuButton);

        return levelMenuButton;
    }

    private ButtonSprite createActivateLaserButton() {
        ButtonSprite activateLaserButton = new ButtonSprite(200, 50, activate_laser_button_region, vbom);
        activateLaserButton.setOnClickListener(new ButtonSprite.OnClickListener() {
            @Override
            public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                activateLaser();
            }
        });

        activateLaserButton.setPosition(720, 400);
        activateLaserButton.setWidth(50);
        activateLaserButton.setHeight(50);
        registerTouchArea(activateLaserButton);

        return activateLaserButton;
    }

    private void activateLaser() {
        disableButtons();
        Log.d("Ref", "Laser activated!.");

        boolean result = drawLaser();
        updateTries();
        if (result) {
            endGame();
        } else {
            if (tryCounter == 0) {
                showLostPopup();
            } else {
                showMissPopup();
                engine.registerUpdateHandler(new TimerHandler(2.0f, false, new ITimerCallback() {
                    @Override
                    public void onTimePassed(TimerHandler pTimerHandler) {
                        clearPreviousLaser();
                        hideMissPopup();
                        enableButtons();
                    }
                }));
            }
        }
        Log.d("Ref", "Laser activation result: " + (result ? "Hit!" : "Miss!"));
    }

    private void updateTries() {
        tryCounter--;
        updateTriesText();
    }

    private void disableButtons() {
        activateLaserButton.setEnabled(false);
        restartLevelButton.setEnabled(false);
        levelMenuButton.setEnabled(false);
    }

    private void enableButtons() {
        activateLaserButton.setEnabled(true);
        restartLevelButton.setEnabled(true);
        levelMenuButton.setEnabled(true);
    }

    private String getFormattedGameTime() {
        return formatDuration(gameDuration);
    }

    private String formatDuration(Duration duration) {
        PeriodFormatter minutesAndSeconds = new PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendMinutes()
                .appendSeparator(":")
                .appendSeconds()
                .toFormatter();
        return minutesAndSeconds.print(duration.toPeriod());
    }

    private String getRating() {
        return (tryCounter + 1) + " stars!";
    }

    private void showLostPopup() {
        overlay = new Rectangle(0, 0, SceneConstants.SCENE_WIDTH, SceneConstants.SCENE_HEIGHT, vbom);
        overlay.setColor(0.5f, 0.5f, 0.5f);
        overlay.setAlpha(0.8f);
        attachChild(overlay);

        Text loseText = new Text(200, 200, font, "You lost! Go back and try again.", new TextOptions(HorizontalAlign.LEFT), vbom);
        overlay.attachChild(loseText);

        ButtonSprite restartButton = createRestartLevelButton();
        restartButton.setPosition(345, 350);
        overlay.attachChild(restartButton);

        ButtonSprite listButton = createLevelMenuButton();
        listButton.setPosition(405, 350);
        overlay.attachChild(listButton);
    }

    private void showMissPopup() {
        missText = new Text(300, 200, font, "You missed!", new TextOptions(HorizontalAlign.LEFT), vbom);
        attachChild(missText);
    }

    private void hideMissPopup() {
        missText.detachSelf();
        missText.dispose();
    }

    private void clearPreviousLaser() {
        for (Line line : laser) {
            line.detachSelf();
            line.dispose();
        }
        laser = new ArrayList<>();
    }

    private boolean drawLaser() {

        boolean hit = false;

        float startX = laserEmitterX;
        float startY = laserEmitterY;

        float currentLaserAngle = laserEmitterAngle;

        boolean ignoreNextCollision = false;

        boolean draw = true;

        do {
            Pair<Float, Float> endPoint = translatePoint(startX, startY, currentLaserAngle);
            float endX = endPoint.first;
            float endY = endPoint.second;
            Line line = createLine(startX, startY, endX, endY);

            if (!isInLevel(line)) {
                Log.d("Ref", "Laser out of bound.");
                draw = false;
            } else {

                Sprite levelObject = checkCollisions(line);
                if (levelObject != null) {
                    LevelObjectType type = (LevelObjectType) levelObject.getUserData();
                    Log.d("Ref", "Collision with: " + type + " angled: " + levelObject.getRotation());
                    switch (type) {
                        case LASER_RECEIVER:
                            draw = false;
                            hit = true;
                            break;
                        case MIRROR:
                            if (!ignoreNextCollision) {
                                ignoreNextCollision = true;
                                float mirrorAngle = levelObject.getRotation();
                                currentLaserAngle = calculateReflectionAngle(mirrorAngle, currentLaserAngle);
                                Log.d("Ref", "New angle: " + currentLaserAngle);
                            } else {
                                Log.d("Ref", "Ignoring Collision with: " + type);
                            }
                            break;
                        case OBSTACLE:
                            draw = false;
                            break;
                    }
                } else {
                    //Log.d("Ref", "Empty space");
                    ignoreNextCollision = false;
                }

                startX = endX;
                startY = endY;
            }
        } while (draw);

        return hit;
    }

    private Line createLine(float startX, float startY, float endX, float endY) {
        Line line = new Line(startX, startY, endX, endY, DEFAULT_LASER_WIDTH, vbom);
        line.setColor(0, 1, 0);

        attachChild(line);
        laser.add(line);

        return line;
    }

    private boolean isInLevel(Line line) {
        float lineX = line.getX2();
        float lineY = line.getY2();

        return !(lineX <= 0 || lineX >= SceneConstants.SCENE_WIDTH) && !(lineY <= 0 || lineY >= SceneConstants.SCENE_HEIGHT);
    }

    private Sprite checkCollisions(Line line) {
        for (Sprite levelObject : levelObjects) {
            if(line.collidesWith(levelObject)) {
                return levelObject;
            }
        }

        return null;
    }

    private void configureScene() {
        setTouchAreaBindingOnActionMoveEnabled(true);
    }

    private void loadLevel() {
        final LevelLoader levelLoader = new LevelLoader();

        levelLoader.registerEntityLoader(LevelConstants.TAG_LEVEL, new IEntityLoader() {
            @Override
            public IEntity onLoadEntity(final String pEntityName, final Attributes pAttributes) {
                //levelWidth = SAXUtils.getIntAttributeOrThrow(pAttributes, LevelConstants.TAG_LEVEL_ATTRIBUTE_WIDTH);
                //levelHeight = SAXUtils.getIntAttributeOrThrow(pAttributes, LevelConstants.TAG_LEVEL_ATTRIBUTE_HEIGHT);

                return GameScene.this;
            }
        });

        levelLoader.registerEntityLoader(EntityConstants.TAG_ENTITY, new IEntityLoader() {
            @Override
            public IEntity onLoadEntity(final String pEntityName, final Attributes pAttributes) {
                final int x = SAXUtils.getIntAttributeOrThrow(pAttributes, EntityConstants.TAG_ENTITY_ATTRIBUTE_X);
                final int y = SAXUtils.getIntAttributeOrThrow(pAttributes, EntityConstants.TAG_ENTITY_ATTRIBUTE_Y);
                final int width = SAXUtils.getIntAttributeOrThrow(pAttributes, EntityConstants.TAG_ENTITY_ATTRIBUTE_WIDTH);
                final int height = SAXUtils.getIntAttributeOrThrow(pAttributes, EntityConstants.TAG_ENTITY_ATTRIBUTE_HEIGHT);
                final int angle = SAXUtils.getIntAttributeOrThrow(pAttributes, EntityConstants.TAG_ENTITY_ATTRIBUTE_ANGLE);
                final String type = SAXUtils.getAttributeOrThrow(pAttributes, EntityConstants.TAG_ENTITY_ATTRIBUTE_TYPE);

                final Sprite levelObject;

                if (type.equals(EntityConstants.TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_LASER_EMITTER)) {
                    levelObject = new Sprite(x, y, laser_emitter_region, vbom);
                    levelObject.setUserData(LevelObjectType.LASER_EMITTER);
                    laserEmitterX = x + width / 2;
                    laserEmitterY = y + height / 2;
                    laserEmitterAngle = angle;
                    Log.d("Ref", "Laser Emitter: (" + laserEmitterX + "," + laserEmitterY + "), angle: " + laserEmitterAngle);
                } else if (type.equals(EntityConstants.TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_LASER_RECEIVER)) {
                    levelObject = new Sprite(x, y, laser_receiver_region, vbom);
                    levelObject.setUserData(LevelObjectType.LASER_RECEIVER);
                    laserReceiverX = x + width / 2;
                    laserReceiverY = y + height / 2;
                    Log.d("Ref", "Laser Receiver: (" + laserReceiverX + "," + laserReceiverY + ")");
                } else if (type.equals(EntityConstants.TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_MIRROR)) {
                    levelObject = new Sprite(x, y, mirror_region, vbom);
                    levelObject.setUserData(LevelObjectType.MIRROR);

                    final Rectangle rect = new Rectangle(x, y, width, height + 100, vbom){

                        float oldTouchY;

                        @Override
                        public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {

                            float touchY = pSceneTouchEvent.getY();
                            switch (pSceneTouchEvent.getAction()) {

                                case TouchEvent.ACTION_DOWN:
                                    oldTouchY = touchY;
                                    setVisible(true);
                                    break;
                                case TouchEvent.ACTION_MOVE:
                                    float changeY = oldTouchY - touchY;

                                    float newAngle = getRotation() - changeY;

                                    setRotation(newAngle);
                                    levelObject.setRotation(newAngle);

                                    oldTouchY = touchY;
                                    break;
                                case TouchEvent.ACTION_UP:
                                    setVisible(false);
                                    break;
                            }
                            return true;
                        }
                    };
                    rect.setAlpha(0.2f);
                    rect.setColor(0, 1, 0);
                    rect.setRotationCenter(width * 0.5f, height * 0.5f);
                    rect.setRotation(angle);
                    rect.setVisible(false);
                    registerTouchArea(rect);
                    attachChild(rect);

                } else if (type.equals(EntityConstants.TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_OBSTACLE1)) {
                    levelObject = new Sprite(x, y, obstacle1_region, vbom);
                    levelObject.setUserData(LevelObjectType.OBSTACLE);
                } else {
                    throw new IllegalArgumentException();
                }

                levelObject.setRotationCenter(width * 0.5f, height * 0.5f);
                levelObject.setRotation(angle);
                levelObject.setHeight(height);
                levelObject.setWidth(width);

                levelObject.setCullingEnabled(true);

                levelObjects.add(levelObject);

                return levelObject;
            }
        });

        try {
            levelLoader.loadLevelFromAsset(activity.getAssets(), "level/" + levelId + ".lvl");
        } catch (final IOException e) {
            Log.e("Ref", "Error loading level.", e);
        }
    }

    private void startGame() {
        gameStartTime = new DateTime();
    }

    private void endGame() {
        calculateGameTime();
        saveScore();
        showWinPopup();
    }

    private void calculateGameTime() {
        DateTime now = new DateTime();
        gameDuration = new Duration(gameStartTime, now);
    }

    private void saveScore() {
        LevelScore levelScore = new LevelScore();
        levelScore.setLevelId(levelId);
        levelScore.setPlayTime(gameDuration.getMillis());
        levelScore.setStars(tryCounter + 1);
        levelScore.setTimestamp(new DateTime().getMillis());

        GameSaveManager.addLevelScoreAndSave(levelScore);
    }

    private void showWinPopup() {
        overlay = new Rectangle(0, 0, SceneConstants.SCENE_WIDTH, SceneConstants.SCENE_HEIGHT, vbom);
        overlay.setColor(0.5f, 0.5f, 0.5f);
        overlay.setAlpha(0.8f);
        attachChild(overlay);

        Text winText = new Text(300, 150, font, "You win!", new TextOptions(HorizontalAlign.LEFT), vbom);
        overlay.attachChild(winText);

        Text timeText = new Text(300, 200, font, "Time taken: " + getFormattedGameTime(), new TextOptions(HorizontalAlign.LEFT), vbom);
        overlay.attachChild(timeText);

        Text ratingText = new Text(300, 250, font, "Rating: " + getRating(), new TextOptions(HorizontalAlign.LEFT), vbom);
        overlay.attachChild(ratingText);

        ButtonSprite restartButton = createRestartLevelButton();
        restartButton.setPosition(345, 350);
        overlay.attachChild(restartButton);

        ButtonSprite listButton = createRestartLevelButton();
        listButton.setPosition(405, 350);
        overlay.attachChild(listButton);

    }

    @Override
    protected void disposeScene() {
        disposeHudAndTexts();

        this.detachSelf();
        this.dispose();
    }

    private void disposeHudAndTexts() {
        levelText.detachSelf();
        levelText.dispose();

        triesText.detachSelf();
        triesText.dispose();

        if (missText != null && !missText.isDisposed()) {
            missText.detachSelf();
            missText.dispose();
        }

        camera.setHUD(null);
        camera.setCenter(400, 240);

        gameHUD.detachSelf();
        gameHUD.dispose();
    }

    @Override
    protected void loadResources() {
        loadTextures();
        loadFonts();
    }

    private void loadTextures() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/game/");
        textureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), SceneConstants.SCENE_WIDTH, SceneConstants.SCENE_HEIGHT, TextureOptions.BILINEAR);

        laser_emitter_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "laser_emitter.png");
        laser_receiver_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "laser_receiver.png");
        obstacle1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "wall.png");
        mirror_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "mirror.png");

        level_list_button_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, activity, "level_list.png", 2, 1);
        restart_level_button_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, activity, "restart_level.png", 2, 1);
        activate_laser_button_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, activity, "go_button.png", 2, 1);

        try {
            textureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
            textureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Log.e("Ref", "So much error on game scene!", e);
        }
    }

    private void loadFonts() {
        FontFactory.setAssetBasePath("font/");
        final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "font.ttf", 50, true, android.graphics.Color.WHITE, 2, android.graphics.Color.BLACK);
        font.load();
    }

    @Override
    protected void unloadResources() {
        textureAtlas.unload();
        laser_emitter_region = null;
        laser_receiver_region = null;
        obstacle1_region = null;
        mirror_region = null;
        activate_laser_button_region = null;
        restart_level_button_region = null;
    }
}
