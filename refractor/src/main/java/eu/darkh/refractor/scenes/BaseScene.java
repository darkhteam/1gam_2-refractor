package eu.darkh.refractor.scenes;

import android.app.Activity;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import eu.darkh.refractor.activities.GameActivity;
import eu.darkh.refractor.managers.RenderingManager;

/**
 * Created by darkh on 2015-02-14.
 */
public abstract class BaseScene extends Scene {

    protected Engine engine;
    protected GameActivity activity;
    protected VertexBufferObjectManager vbom;
    protected Camera camera;

    protected BuildableBitmapTextureAtlas textureAtlas;

    public BaseScene() {
        this.engine = RenderingManager.engine;
        this.activity = RenderingManager.activity;
        this.vbom = RenderingManager.vbom;
        this.camera = RenderingManager.camera;
    }

    public void loadResourcesAndCreateScene(){
        loadResources();
        createScene();
    }

    public void unloadResourcesAndDisposeScene(){
        disposeScene();
        unloadResources();
    }

    public abstract void onBackKeyPressed();

    protected abstract void createScene();

    protected abstract void disposeScene();

    protected abstract void loadResources();

    protected abstract void unloadResources();
}
