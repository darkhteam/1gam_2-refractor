package eu.darkh.refractor.scenes;

import android.util.Log;
import android.util.Pair;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.animator.AlphaMenuAnimator;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import eu.darkh.refractor.consts.SceneConstants;
import eu.darkh.refractor.managers.GameSaveManager;
import eu.darkh.refractor.managers.SceneManager;
import eu.darkh.refractor.model.LevelScore;

/**
 * Created by darkh on 2015-02-14.
 */
public class LevelMenuScene extends BaseScene implements MenuScene.IOnMenuItemClickListener {

    private ITextureRegion level_button_region;
    private ITextureRegion star_gold_region;
    private ITextureRegion star_silver_region;
    private ITextureRegion star_bronze_region;

    private MenuScene menuChildScene;
    private HashMap<Integer, LevelScore> scores;

    private Font font;

    @Override
    public void onBackKeyPressed() {
        activity.finish();
    }

    @Override
    protected void createScene() {
        setBackground(new Background(Color.WHITE));
        createMenuChildScene();
    }

    @Override
    protected void disposeScene() {
        menuChildScene.detachSelf();
        menuChildScene.dispose();
    }

    @Override
    protected void loadResources() {
        loadTextures();
        loadFonts();
    }

    private void loadTextures() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
        textureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), SceneConstants.SCENE_WIDTH, SceneConstants.SCENE_HEIGHT, TextureOptions.BILINEAR);
        level_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "level_button.png");
        star_gold_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "star_gold.png");
        star_silver_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "star_silver.png");
        star_bronze_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, activity, "star_bronze.png");

        try {
            textureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
            textureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Log.e("Ref", "So much error on menu scene!", e);
        }
    }

    @Override
    protected void unloadResources() {
        textureAtlas.unload();
        level_button_region = null;
    }

    private void createMenuChildScene() {

        int row = 1;
        int column = 1;

        menuChildScene = new MenuScene(camera);
        menuChildScene.setMenuAnimator(new AlphaMenuAnimator(HorizontalAlign.LEFT));
        menuChildScene.setPosition(0, 0);

        scores = GameSaveManager.getLevelScores();

        ArrayList<IMenuItem> levelItems = new  ArrayList<IMenuItem>();

        try {

            int levelCount = activity.getAssets().list("level").length;

            for (int i = 0; i < levelCount; i++) {
                IMenuItem playMenuItem = createMenuItem(i);

                menuChildScene.addMenuItem(playMenuItem);
                levelItems.add(playMenuItem);
            }

            menuChildScene.buildAnimations();
            menuChildScene.setBackgroundEnabled(false);

            for (int i = 0; i < levelCount; i++) {
                IMenuItem playMenuItem = levelItems.get(i);

                float posX = SceneConstants.LEVEL_ICON_MARGIN * row + (playMenuItem.getWidth() * (row - 1));

                //check if not new line
                if (posX > (SceneConstants.SCENE_WIDTH - SceneConstants.LEVEL_ICON_MARGIN)) {
                    row = 1;
                    column++;

                    posX = SceneConstants.LEVEL_ICON_MARGIN * row + (playMenuItem.getWidth() * (row - 1));
                }

                float posY = SceneConstants.LEVEL_ICON_MARGIN * column + (playMenuItem.getHeight() * (column - 1));

                playMenuItem.setPosition(posX, posY);
                row++;
            }

        } catch (IOException e) {
            Log.e("Ref", "Can't load level file", e);
        }

        menuChildScene.setOnMenuItemClickListener(this);
        setChildScene(menuChildScene);
    }

    private IMenuItem createMenuItem(int position) {

        String levelStr = String.valueOf(position + 1);

        IMenuItem playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(position, level_button_region, vbom), 1.2f, 1);

        float xPositionMulti = 2f/5f;

        if (levelStr.length() > 1) {
            xPositionMulti = 3f/9f;
        }

        Text levelText = new Text(playMenuItem.getWidth() * xPositionMulti, playMenuItem.getHeight()/4, font, levelStr, new TextOptions(HorizontalAlign.LEFT), vbom);

        playMenuItem.attachChild(levelText);

        LevelScore levelScore = scores.get(position);
        if (levelScore != null) {
            int stars = levelScore.getStars();

            Sprite star = createStarSprite(stars);

            playMenuItem.attachChild(star);

        }

        return playMenuItem;
    }

    private Sprite createStarSprite(int stars) {
        Sprite star;
        if (stars == 3) {
            star = new Sprite(48, 48, star_gold_region, vbom);
        } else if (stars == 2) {
            star = new Sprite(48, 48, star_silver_region, vbom);
        } else {
            star = new Sprite(48, 48, star_bronze_region, vbom);
        }
        star.setPosition(1.0f, 0.0f);

        return star;
    }

    @Override
    public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY) {
        int levelId = pMenuItem.getID();
        Log.d("Ref", "level selected: " + levelId);
        loadGameScene(levelId);
        return true;
    }

    private void loadGameScene(int levelId) {
        SceneManager.getInstance().loadGameScene(levelId, engine);
    }

    private void loadFonts() {
        FontFactory.setAssetBasePath("font/");
        final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "font.ttf", 50, true, android.graphics.Color.WHITE, 2, android.graphics.Color.BLACK);
        font.load();
    }
}
