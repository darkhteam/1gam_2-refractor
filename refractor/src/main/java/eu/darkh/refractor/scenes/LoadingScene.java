package eu.darkh.refractor.scenes;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.color.Color;

/**
 * Created by darkh on 2015-02-14.
 */
public class LoadingScene extends BaseScene {

    public Font font;

    @Override
    public void onBackKeyPressed() {

    }

    @Override
    protected void createScene() {
        setBackground(new Background(Color.WHITE));
        attachChild(new Text(250, 200, font, "Loading...", vbom));
    }

    @Override
    protected void disposeScene() {

    }

    @Override
    protected void loadResources() {
        loadFonts();
    }

    private void loadFonts() {
        FontFactory.setAssetBasePath("font/");
        final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "font.ttf", 50, true, android.graphics.Color.WHITE, 2, android.graphics.Color.BLACK);
        font.load();
    }

    @Override
    protected void unloadResources() {

    }
}
