package eu.darkh.refractor.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import eu.darkh.library.utils.StorageUtils;
import eu.darkh.refractor.model.LevelScore;

/**
 * Created by darkh on 2015-02-15.
 */
public class GameSaveManager {

    private static final String GAME_SAVE_KEY = "game_save";

    private static HashMap<Integer, LevelScore> scores = new HashMap<Integer, LevelScore>();

    private static Gson gson;

    public static void prepareManager(){
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
    }

    public static void saveGameSave() {
        saveObject(GAME_SAVE_KEY, scores);
    }

    private static void saveObject(String key, Object o) {
        String JSON = gson.toJson(o);
        StorageUtils.putStringInSharedPreferences(key, JSON);
    }

    public static void tryToLoadGameSave() {
        try {
            loadGameSave();
        } catch (Exception ignored) {

        }
    }

    private static void loadGameSave() {
        Type type = new TypeToken<HashMap<Integer, LevelScore>>(){}.getType();

        String scoresJSON = StorageUtils.getStringFromSharedPreferences(GAME_SAVE_KEY);

        scores = gson.fromJson(scoresJSON, type);

        if (scores == null) {
            scores = new HashMap<>();
        }
    }

    public static void addLevelScoreAndSave(LevelScore levelScore) {
        addLevelScore(levelScore);
        saveGameSave();
    }

    public static void addLevelScore(LevelScore levelScore) {
        int position = levelScore.getLevelId();
        scores.put(position, levelScore);
    }

    public static HashMap<Integer, LevelScore> getLevelScores() {
        tryToLoadGameSave();
        return scores;
    }

}
