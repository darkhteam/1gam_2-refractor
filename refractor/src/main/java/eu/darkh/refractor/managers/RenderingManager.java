package eu.darkh.refractor.managers;

import android.app.Activity;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import eu.darkh.refractor.activities.GameActivity;

/**
 * Created by darkh on 2015-02-14.
 */
public class RenderingManager {
    public static Engine engine;
    public static GameActivity activity;
    public static VertexBufferObjectManager vbom;
    public static Camera camera;

    public static void prepareManager(Engine engine, GameActivity activity, Camera camera, VertexBufferObjectManager vbom) {
        RenderingManager.engine = engine;
        RenderingManager.activity = activity;
        RenderingManager.camera = camera;
        RenderingManager.vbom = vbom;
    }
}
