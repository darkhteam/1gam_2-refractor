package eu.darkh.refractor.managers;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.ui.IGameInterface;

import eu.darkh.refractor.scenes.BaseScene;
import eu.darkh.refractor.scenes.GameScene;
import eu.darkh.refractor.scenes.LevelMenuScene;
import eu.darkh.refractor.scenes.LoadingScene;
import eu.darkh.refractor.scenes.SplashScene;

/**
 * Created by darkh on 2015-02-14.
 */
public class SceneManager {

    private static SceneManager INSTANCE;

    private BaseScene splashScene;
    private BaseScene levelMenuScene;
    private BaseScene gameScene;
    private BaseScene loadingScene;

    private BaseScene currentScene;

    private SceneManager() {
    }

    public static SceneManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SceneManager();
        }
        return INSTANCE;
    }

    public void setSplashSceneAsCurrent() {
        setSceneAsCurrent(splashScene);
    }

    public void setLevelMenuSceneAsCurrent() {
        setSceneAsCurrent(levelMenuScene);
    }

    public void setGameSceneAsCurrent() {
        setSceneAsCurrent(gameScene);
    }

    public void setLoadingSceneSceneAsCurrent() {
        setSceneAsCurrent(loadingScene);
    }

    private void setSceneAsCurrent(BaseScene scene) {
        RenderingManager.engine.setScene(scene);
        currentScene = scene;
    }

    public BaseScene getCurrentScene() {
        return currentScene;
    }

    public void createSplashScene(IGameInterface.OnCreateSceneCallback onCreateSceneCallback) {
        splashScene = new SplashScene();
        splashScene.loadResourcesAndCreateScene();
        currentScene = splashScene;
        onCreateSceneCallback.onCreateSceneFinished(splashScene);
    }

    public void createLevelMenuScene() {
        loadingScene = new LoadingScene();
        loadingScene.loadResourcesAndCreateScene();
        levelMenuScene = new LevelMenuScene();
        levelMenuScene.loadResourcesAndCreateScene();
        setLevelMenuSceneAsCurrent();
        disposeSplashScene();
    }

    public void loadLevelMenuScene() {
        gameScene.unloadResourcesAndDisposeScene();
        setLevelMenuSceneAsCurrent();
    }

    public void loadGameScene(final int levelId, final Engine mEngine) {
        setLoadingSceneSceneAsCurrent();
        mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() {
            public void onTimePassed(final TimerHandler pTimerHandler) {
                mEngine.unregisterUpdateHandler(pTimerHandler);
                createGameScene(levelId);
            }
        }));
    }

    public void createGameScene(int levelId) {
        gameScene = new GameScene(levelId);
        gameScene.loadResourcesAndCreateScene();
        setGameSceneAsCurrent();
    }

    public void restartLevel(int levelId, Engine engine) {
        gameScene.unloadResourcesAndDisposeScene();
        loadGameScene(levelId, engine);
    }

    private void disposeSplashScene() {
        splashScene.unloadResourcesAndDisposeScene();
        splashScene = null;
    }

    private void disposeLevelMenuScene() {
        levelMenuScene.unloadResourcesAndDisposeScene();
        levelMenuScene = null;
    }

    private void disposeGameScene() {
        gameScene.unloadResourcesAndDisposeScene();
        gameScene = null;
    }
}
