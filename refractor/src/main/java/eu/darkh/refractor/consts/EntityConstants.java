package eu.darkh.refractor.consts;

/**
 * Created by darkh on 2015-02-22.
 */
public class EntityConstants {

    public static final String TAG_ENTITY = "entity";
    public static final String TAG_ENTITY_ATTRIBUTE_X = "x";
    public static final String TAG_ENTITY_ATTRIBUTE_Y = "y";
    public static final String TAG_ENTITY_ATTRIBUTE_WIDTH = "width";
    public static final String TAG_ENTITY_ATTRIBUTE_HEIGHT = "height";
    public static final String TAG_ENTITY_ATTRIBUTE_ANGLE = "angle";
    public static final String TAG_ENTITY_ATTRIBUTE_TYPE = "type";

    public static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_LASER_EMITTER = "laser_emitter";
    public static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_LASER_RECEIVER = "laser_receiver";
    public static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_MIRROR = "mirror";
    public static final Object TAG_ENTITY_ATTRIBUTE_TYPE_VALUE_OBSTACLE1 = "obstacle1";

}
