package eu.darkh.refractor.consts;

/**
 * Created by Aleksander on 2015-03-09.
 */
public class SceneConstants {

    public static final int LEVEL_ICON_MARGIN = 30;
    public static final int SCENE_WIDTH = 800;
    public static final int SCENE_HEIGHT = 480;
}
