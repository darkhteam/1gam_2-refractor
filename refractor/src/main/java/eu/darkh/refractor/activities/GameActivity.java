package eu.darkh.refractor.activities;

import android.os.Bundle;
import android.view.KeyEvent;

import com.crashlytics.android.Crashlytics;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;

import org.andengine.ui.activity.BaseGameActivity;

import eu.darkh.library.utils.StorageUtils;
import eu.darkh.refractor.consts.Constants;
import eu.darkh.refractor.managers.GameSaveManager;
import eu.darkh.refractor.managers.RenderingManager;
import eu.darkh.refractor.managers.SceneManager;
import io.fabric.sdk.android.Fabric;

/**
 * Created by win8 on 2015-02-13.
 */
public class GameActivity extends BaseGameActivity {

    private static String SHARED_PREFERENCES_FILE = "maptriviadata";
    private Camera camera;

    //To be moved probably to string values
    float WIDTH = 800;
    float HEIGHT = 480;

    protected void onCreate(Bundle savedInstanceState) {
        initComponents();

        super.onCreate(savedInstanceState);
    }

    @Override
    public Engine onCreateEngine(EngineOptions engineOptions) {
        return new LimitedFPSEngine(engineOptions, 60);
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback onCreateResourcesCallback) throws Exception {
        RenderingManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
        onCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        camera = new Camera(0, 0, WIDTH, HEIGHT);
        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(WIDTH, HEIGHT), this.camera);
        engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
        engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
        return engineOptions;
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback onCreateSceneCallback) throws Exception {
        SceneManager.getInstance().createSplashScene(onCreateSceneCallback);
    }

    @Override
    public void onPopulateScene(Scene scene, OnPopulateSceneCallback onPopulateSceneCallback) throws Exception {
        mEngine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
            public void onTimePassed(final TimerHandler pTimerHandler) {
                mEngine.unregisterUpdateHandler(pTimerHandler);
                // load menu resources, create menu scene
                // set menu scene using scene manager
                // disposeSplashScene();
                // READ NEXT ARTICLE FOR THIS PART.
            }
        }));
        onPopulateSceneCallback.onPopulateSceneFinished();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
        }
        return false;
    }

    private void initComponents() {
        initCrashlytics();
        GameSaveManager.prepareManager();
        StorageUtils.initStorageUtils(getBaseContext(), SHARED_PREFERENCES_FILE);
    }

    private void initCrashlytics() {
        if (!Constants.IS_DEVELOPMENT) {
            Fabric.with(this, new Crashlytics());
        }
    }
}
