package eu.darkh.refractor.utils;

import android.util.Pair;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by darkh on 2015-03-01.
 */
public class MathUtils {

    public static Pair<Float, Float> translatePoint(float x0, float y0, float angle) {

        double y = y0 + 2.0;

        double alfa = (angle * Math.PI) / 180;

        double sin = Math.sin(alfa);
        double cos = Math.cos(alfa);

        float x1 = (float)(-((y - y0) * sin) + x0);
        float y1 = (float)(((y - y0) * cos) + y0);

        return new Pair<>(x1, y1);
    }

    public static float calculateReflectionAngle(float mirrorAngle, float laserAngle) {
        return (((mirrorAngle - laserAngle) * 2 + laserAngle) + 180) % 360;
    }
}
